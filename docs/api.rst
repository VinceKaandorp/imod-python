imod package
============

imod module
-----------

.. automodule:: imod
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

imod.idf module
---------------

.. automodule:: imod.idf
    :members:
    :undoc-members:
    :exclude-members: dask, dataarray
    :show-inheritance:

imod.ipf module
---------------

.. automodule:: imod.ipf
    :members:
    :undoc-members:
    :show-inheritance:

imod.prepare module
-------------------

.. automodule:: imod.prepare
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:

imod.rasterio module
--------------------

.. automodule:: imod.rasterio
    :members:
    :undoc-members:
    :show-inheritance:

imod.run module
---------------

.. automodule:: imod.run
    :members:
    :undoc-members:
    :show-inheritance:

imod.tec module
---------------

.. automodule:: imod.tec
    :members:
    :undoc-members:
    :show-inheritance:

imod.util module
----------------

.. automodule:: imod.util
    :members:
    :undoc-members:
    :show-inheritance:

imod.wq module
--------------

.. automodule:: imod.wq
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
