Indexing
========

Data preparation: specific selection with ``.where``, boolean arrays, ``binary_dilation``, ``binary_erosion``, ``scikit.ndimage.morphology``, etc.
